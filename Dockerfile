FROM python:3.8-slim-buster
COPY . /tmp/
WORKDIR /tmp/
#RUN apt install openpyxl
RUN pip3 install ./pkgs/*
RUN rm -rf ./pkgs/*
ENTRYPOINT ["python3","Amazon_Scraper.py"]
